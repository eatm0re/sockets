package edu.eatmore.sockets.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Receiver extends Thread {

  private Socket socket;
  private InputStream inputStream;
  private Scanner inputScanner;

  public Receiver(Socket socket) throws IOException {
    this.socket = socket;
    inputStream = socket.getInputStream();
    inputScanner = new Scanner(inputStream);
  }

  @Override
  public void run() {
    try {
      while (!interrupted()) {
        String message = inputScanner.nextLine();
        System.out.println(message);
      }
    } catch (NoSuchElementException e) {
      System.out.println("Disconnected");
    } finally {
      try {
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
