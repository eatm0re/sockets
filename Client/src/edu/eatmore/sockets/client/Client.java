package edu.eatmore.sockets.client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Properties;
import java.util.Scanner;

public class Client {

  private static final String PROPERTIES_FILE = "client.properties";
  private static final String PROPERTY_ENDPOINT_HOST = "endpoint.host";
  private static final String PROPERTY_ENDPOINT_PORT = "endpoint.port";

  public static void main(String[] args) {
    try (Socket socket = new Socket()) {
      initEndpoint(socket);
      PrintStream outputPrintStream = new PrintStream(socket.getOutputStream());
      new Receiver(socket).start();
      Scanner inputScanner = new Scanner(System.in);
      for (String message = inputScanner.nextLine(); !message.equalsIgnoreCase("exit"); message = inputScanner.nextLine()) {
        outputPrintStream.println(message);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void initEndpoint(Socket socket) throws IOException {
    Properties clientProperties = new Properties();
    clientProperties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(PROPERTIES_FILE));
    try {
      clientProperties.load(new FileInputStream(PROPERTIES_FILE));
    } catch (FileNotFoundException ignored) {}
    String endpointHost = clientProperties.getProperty(PROPERTY_ENDPOINT_HOST);
    int endpointPort = Integer.parseInt(clientProperties.getProperty(PROPERTY_ENDPOINT_PORT));
    socket.connect(new InetSocketAddress(endpointHost, endpointPort));
  }
}
