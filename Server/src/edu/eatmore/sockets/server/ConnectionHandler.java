package edu.eatmore.sockets.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionHandler extends Thread {

  private Server server;

  public ConnectionHandler(Server server) {
    this.server = server;
  }

  @Override
  public void run() {
    System.out.println("Connection handler has started!");
    try (ServerSocket socket = new ServerSocket(23000)) {
      while (true) {
        Socket clientSocket = socket.accept();
        Client client = new Client(server, clientSocket);
        server.acceptClient(client);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
