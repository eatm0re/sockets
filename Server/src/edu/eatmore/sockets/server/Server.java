package edu.eatmore.sockets.server;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class Server extends Thread {

  private final List<Client> clients = new ArrayList<>();
  private final Queue<String> messagesQueue = new ArrayDeque<>();

  public static void main(String[] args) {
    System.out.println("Starting server...");
    Server server = new Server();
    server.start();
    System.out.println("Starting connection handler...");
    new ConnectionHandler(server).start();
  }

  public void acceptClient(Client client) {
    System.out.println("Accepting new client...");
    synchronized (clients) {
      clients.add(client);
    }
    client.start();
  }

  public void removeClient(Client client) {
    synchronized (clients) {
      clients.remove(client);
    }
  }

  public void sendToAll(String message) {
    synchronized (messagesQueue) {
      messagesQueue.add(message);
    }
    this.interrupt();
  }

  @Override
  public void run() {
    System.out.println("Server has started!");
    while (true) {
      while (!messagesQueue.isEmpty()) {
        String message = pollMessage();
        for (Client client : clients) {
          client.sendMessage(message);
        }
      }
      try {
        sleep(10000);
      } catch (InterruptedException ignored) {}
    }
  }

  private String pollMessage() {
    synchronized (messagesQueue) {
      return messagesQueue.poll();
    }
  }
}
