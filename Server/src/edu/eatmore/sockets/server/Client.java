package edu.eatmore.sockets.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Client extends Thread {

  private Server server;
  private Socket socket;
  private String username;
  private InputStream inputStream;
  private Scanner inputScanner;
  private OutputStream outputStream;
  private PrintStream outputPrintStream;

  public Client(Server server, Socket socket) throws IOException {
    this.server = server;
    this.socket = socket;
    inputStream = socket.getInputStream();
    inputScanner = new Scanner(inputStream);
    outputStream = socket.getOutputStream();
    outputPrintStream = new PrintStream(outputStream);
  }

  @Override
  public void run() {
    try {
      username = initUsername();
      handleMessage(constructMessage(String.format("%s has connected. Welcome!", username)));
      while (!interrupted()) {
        String rawMessage = receiveMessage();
        String constructedMessage = constructMessage(rawMessage);
        handleMessage(constructedMessage);
      }
    } catch (NoSuchElementException e) {
      handleMessage(constructMessage("Disconnected."));
    } finally {
      server.removeClient(this);
      try {
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void sendMessage(String message) {
    outputPrintStream.print(message);
  }

  private String receiveMessage() {
    return inputScanner.nextLine();
  }

  private String constructMessage(String message) {
    return String.format("[%tT] (%s): %s%n", Calendar.getInstance(), username, message);
  }

  private void handleMessage(String message) {
    System.out.print(message);
    server.sendToAll(message);
  }

  private String initUsername() {
    //return socket.getInetAddress().toString();
    sendMessage("Type your name please:\n");
    return receiveMessage();
  }
}
